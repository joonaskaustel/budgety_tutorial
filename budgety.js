//BUDGET CONTROLLER
var budgetController = (function(){

  var Expense = function(id, description, value){
    this.id = id;
    this.description = description;
    this.value = value;
  }

  var Income = function(id, description, value){
    this.id = id;
    this.description = description;
    this.value = value; 
  }

  var calculateTotal = function(type){
    var sum = 0;

    data.allItems[type].forEach(function(cur){
      sum = sum + cur.value;
    });

    data.totals[type] = sum;
  }

  var data = {
    allItems: {
      exp: [],
      inc: []
    },
    totals: {
      exp: 0,
      inc: 0
    },
    budget: 0,
    percentage: -1
  }

  return {
    addItem: function (type, desc, val) {
      var newItem, ID;
      ID = 0;

      if (data.allItems[type].length > 0) {
        ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
      } else {
        ID = 0;
      }

      if (type === 'exp') {
        newItem = new Expense(ID, desc, val);
      } else if (type === "inc"){
        newItem = new Income (ID, desc, val);
      }

      data.allItems[type].push(newItem);
      return newItem;

    },

    deleteItem: function(type, id){
      var ids, index;

      ids = data.allItems[type].map(function(current){
        return current.id;
      });

      index = ids.indexOf(id);

      if (index !== -1) {
        data.allItems[type].splice(index, 1);
      }

    },

    calculateBudget: function(){
      //Calculate total income and exprense
      calculateTotal('exp');
      calculateTotal('inc');

      //Calculate the budget: income - expenses
      data.budget = data.totals.income - data.totals.expense;

      if (data.totals.income > 0) {
        //Calculate the percentage of income that is spent
        data.percentage = Math.round(data.totals.expense / data.totals.income);
      } else {
        data.percentage = -1;
      }
    },

    getBudget: function(){
      return {
        budget: data.budget,
        totalInc: data.totals.income,
        totalExp: data.totals.expense,
        percentage: data.percentage
      }
    },

    testing:function(){
      console.log(data);
    } 
  }

})();

//UI CONTROLLER
var UIController = (function() {

  var DOMStrings = {
    inputType: '.add__type',
    inputDescription: '.add__description',
    inputValue: '.add__value',
    addBtn: '.add__btn',
    incomeContainer: '.income__list',
    expenseContainer: '.expenses__list',
    budgetLable: '.budget__value',
    incomeLabel: '.budget__income--value',
    expenseLabel: '.budget__expenses--value',
    percentageLabel: '.budget__expenses--percentage',
    container: '.container'
  }

  return {
    getInput: function() {
      return {
        type : document.querySelector(DOMStrings.inputType).value, // value will be 'inc' or 'exp'
        description : document.querySelector(DOMStrings.inputDescription).value,
        value : parseFloat(document.querySelector(DOMStrings.inputValue).value)
      }
    }, 

    addListItem: function(obj, type){
      var html, newHtml;
      
      //Create a html strin with placeholder text
      if (type === 'inc') {
        element = DOMStrings.incomeContainer;
        html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'
      } else {
        element = DOMStrings.expenseContainer;
        html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'
      }

      //replace placeholder text with data
      newHtml = html.replace('%id%', obj.id);
      newHtml = newHtml.replace('%description%', obj.description);
      newHtml = newHtml.replace('%value%', obj.value);

      //insert html into the dom
      document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);

    },

    deleteListItem: function(selectorID){
      var el = document.getElementById(selectorID);

      el.parentNode.removeChild(el);
    },

    clearFields: function() {
      var fields, fieldsArray;

      fields = document.querySelectorAll(DOMStrings.inputDescription + ', ' + DOMStrings.inputValue);

      fieldsArray = Array.prototype.slice.call(fields);

      fieldsArray.forEach(function(current, index, array){
        current.value = "";
      });

      fieldsArray[0].focus();

    },

    displayBudget: function(obj){
      document.querySelector(DOMStrings.budgetLable).textContent = obj.budget;
      document.querySelector(DOMStrings.incomeLabel).textContent = obj.totalInc;
      document.querySelector(DOMStrings.expenseLabel).textContent = obj.totalExp;
      
      if (obj.percentage > 0) {
        document.querySelector(DOMStrings.percentageLabel).textContent = obj.percentage = + '%';
      } else {
        document.querySelector(DOMStrings.percentageLabel).textContent = '---';
      }

    },

    getDOMStrings: function(){
      return DOMStrings;
    }
  }

})();

//GLOBAL CONTROLLER
var controller = (function(budgetCtrl, UICntrl){

  var setUpEventListeners = function(){
    var DOM = UICntrl.getDOMStrings();

    //When add button is clicked
    document.querySelector(DOM.addBtn).addEventListener('click', ctrlAddItem);

    //When enter is pressed
    document.addEventListener('keypress', function(event){
      if (event.keyCode === 13 || event.which === 13) {
        ctrlAddItem();
      }
    });

    document.querySelector(DOM.container).addEventListener('click', controlDeleteItem);
  }

  var updateBudget = function(){
    
    // Calculate the budget
    budgetController.calculateBudget();

    //Return budget
    var budget = budgetController.getBudget();

    // Display the budget in ui
    UIController.displayBudget(budget);
  }

  var updatePercentages = function(){

  };

  var ctrlAddItem = function(){
    var input, newItem;

    //1. Get input data
    input = UICntrl.getInput();
    
    if (input.description !== "" && !isNaN(input.value) && input.value > 0) {
          //2. Add the item to the budget controller
          newItem = budgetController.addItem(input.type, input.description, input.value);
      
          //3. Add the new item to the user interface as well
          UICntrl.addListItem(newItem, input.type);
      
          //Clear the fields
          UICntrl.clearFields();

          updateBudget();

          updatePercentages();
    }

  }

  var controlDeleteItem = function(event){
    var itemID, splitID, type, id;

    itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
    
    if (itemID ) {
      //Inc-1
      splitID = itemID.split('-');
      type = splitID[0];
      id = parseInt(splitID[1]);

      console.log(type, id);
      
      budgetController.deleteItem(type, id);

      UICntrl.deleteListItem(itemID);

      updateBudget();

      updatePercentages();
    }

  }

  return {
    init: function(){
      console.log('application started');
      setUpEventListeners();
    }
  }

})(budgetController, UIController);

controller.init();